import * as types from "../constants/ActionTypes"
import { User } from "../components/Table"

export type AppActions =
  | SelectUserAction
  | SelectUsersAction
  | RemoveSelectedUsersAction
  | RemoveSelectedUserAction
  | IncrementRowAction
  | DecrementRowAction
  | UpUserAction
  | DownUserAction
  | EditUserAction

interface SelectUserAction {
  type: types.types.SELECT_USER
  id: string
}
export const selectUser = (id: string): SelectUserAction => ({
  type: types.types.SELECT_USER,
  id
})

interface SelectUsersAction {
  type: types.types.SELECT_USERS
  users: User[]
}

export const selectUsers = (users: User[]): SelectUsersAction => ({
  type: types.types.SELECT_USERS,
  users
})

interface RemoveSelectedUsersAction {
  type: types.types.REMOVE_SELECTED_USERS
  users: User[]
}

export const removeSelectedUsers = (
  users: User[]
): RemoveSelectedUsersAction => ({
  type: types.types.REMOVE_SELECTED_USERS,
  users
})

interface RemoveSelectedUserAction {
  type: types.types.REMOVE_SELECTED_USER
  id: string
}

export const removeSelectedUser = (id: string): RemoveSelectedUserAction => ({
  type: types.types.REMOVE_SELECTED_USER,
  id
})

interface IncrementRowAction {
  type: types.types.INCREMENT_ROW
  firstName: string
  lastName: string
  age: number
  favouriteColor: string
  captain: string
}

export const incrementRow = (
  firstName: string,
  lastName: string,
  age: number,
  favouriteColor: string,
  captain: string
): IncrementRowAction => ({
  type: types.types.INCREMENT_ROW,
  firstName,
  lastName,
  age,
  favouriteColor,
  captain
})

interface DecrementRowAction {
  type: types.types.DECREMENT_ROW
  id: string
}

export const decrementRow = (id: string): DecrementRowAction => ({
  type: types.types.DECREMENT_ROW,
  id
})

interface UpUserAction {
  type: types.types.UP_USER
  id: string
}
export const upUser = (id: string): UpUserAction => ({
  type: types.types.UP_USER,
  id
})

interface DownUserAction {
  type: types.types.DOWN_USER
  id: string
}

export const downUser = (id: string): DownUserAction => ({
  type: types.types.DOWN_USER,
  id
})

interface EditUserAction {
  type: types.types.EDIT_USER
  id: string
  editedUser: {
    firstName: string
    lastName: string
    age: number
    favouriteColor: string
    captain: string
  }
}

export const editUser = (
  id: string,
  editedUser: {
    firstName: string
    lastName: string
    age: number
    favouriteColor: string
    captain: string
  }
): EditUserAction => ({
  type: types.types.EDIT_USER,
  id,
  editedUser
})
