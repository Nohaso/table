import React, { Component } from "react"
import "./App.css"
import { CreateTable, User } from "./components/Table"
import { CreateList } from "./components/UserList"
import { Route, Link, Switch } from "react-router-dom"
import { AddUser } from "./components/AddUser"
import { ColumnGraph } from "./components/ColumnGraph"
import { CreateTable2 } from "./components/Table2"
import {
  connect,
  MapDispatchToProps,
  MapDispatchToPropsFunction,
  MapStateToProps
} from "react-redux"
import * as actions from "./actions"

export interface State {
  users: User[]
  selectedUsersIds: string[]
}

export interface DispatchFromProps {
  selectUser: typeof actions.selectUser
  selectUsers: typeof actions.selectUsers
  removeUser: typeof actions.removeSelectedUser
  removeSelectedUsers: typeof actions.removeSelectedUsers
  incrementRow: typeof actions.incrementRow
  decrementRow: typeof actions.decrementRow
  upUser: typeof actions.upUser
  downUser: typeof actions.downUser
  editUser: typeof actions.editUser
}

export interface StateFromProps {
  users: User[]
  selectedUsersIds: string[]
}

interface WithFavouriteColour {
  favouriteColor: string
}

interface WithCaptain {
  captain: string
  id: string
}

class App extends React.Component<DispatchFromProps & StateFromProps, State> {
  groupByCaptain = (objectArray: WithCaptain[]) => {
    return objectArray.reduce<Record<string, WithCaptain[]>>((acc, obj) => {
      var key = 0
      objectArray.forEach(object => {
        if (obj.id === object.captain) key++
      })
      if (!acc[key]) {
        acc[key] = []
      }
      acc[key].push(obj)
      return acc
    }, {})
  }

  groupByColour = (objectArray: WithFavouriteColour[]) => {
    return objectArray.reduce<Record<string, WithFavouriteColour[]>>(
      (acc, obj) => {
        var key = obj.favouriteColor
        if (!acc[key]) {
          acc[key] = []
        }
        acc[key].push(obj)
        return acc
      },
      {}
    )
  }

  render() {
    const {
      selectUser,
      selectUsers,
      removeUser,
      removeSelectedUsers,
      incrementRow,
      decrementRow,
      upUser,
      downUser,
      editUser,
      users,
      selectedUsersIds
    } = this.props

    return (
      <div className="App">
        <div>
          <div>
            <Link to="/users/add">AddUser</Link>
          </div>
          <div>
            <Link to="/users">Users List</Link>
          </div>
          <div>
            <Link to="/overview/rels">Rels Overview</Link>
          </div>
          <div>
            <Link to="/overview/colours">Colours Overview</Link>
          </div>
        </div>
        <div>
          <Switch>
            <Route
              path="/users/add"
              render={props => (
                <AddUser
                  {...props}
                  addUser={incrementRow}
                  editUser={editUser}
                  users={users}
                />
              )}
            />
            <Route
              path="/users/:id"
              render={props => (
                <AddUser
                  {...props}
                  addUser={incrementRow}
                  editUser={editUser}
                  users={users}
                />
              )}
            />
            <Route
              path="/users"
              render={props => (
                <CreateTable
                  {...props}
                  users={users}
                  decrementRow={decrementRow}
                  upUser={upUser}
                  downUser={downUser}
                />
              )}
            />
            <Route
              path="/ref/:id"
              render={props => (
                <CreateList
                  {...props}
                  users={users}
                  width={600}
                  height={600}
                  selectUser={selectUser}
                  removeUser={removeUser}
                  selectedUsersIds={selectedUsersIds}
                />
              )}
            />
            <Route
              path="/overview/colours/:users"
              render={props => (
                <CreateTable2 {...props} users={users} colours={true} />
              )}
            />
            <Route
              path="/overview/colours"
              render={props => (
                <ColumnGraph
                  {...props}
                  users={users}
                  width={600}
                  height={600}
                  remove={removeSelectedUsers}
                  select={selectUsers}
                  sortby={this.groupByColour}
                />
              )}
            />
            <Route
              path="/overview/rels/:users"
              render={props => (
                <CreateTable2 {...props} users={users} colours={false} />
              )}
            />
            <Route
              path="/overview/rels"
              render={props => (
                <ColumnGraph
                  {...props}
                  users={users}
                  width={600}
                  height={600}
                  remove={removeSelectedUsers}
                  select={selectUsers}
                  sortby={this.groupByCaptain}
                />
              )}
            />
          </Switch>
        </div>
      </div>
    )
  }
}
type ReduxState = MapStateToProps<any, {}, State>
const mapStateToProps: ReduxState = state => {
  return {
    users: state.users,
    selectedUsersIds: state.selectedUsersIds
  }
}

type ReduxDispatch = MapDispatchToPropsFunction<DispatchFromProps, {}>
const mapDispatchToProps: ReduxDispatch = dispatch => ({
  selectUser: (id: string) => dispatch(actions.selectUser(id)),
  selectUsers: (users: User[]) => dispatch(actions.selectUsers(users)),
  removeUser: (id: string) => dispatch(actions.removeSelectedUser(id)),
  removeSelectedUsers: (users: User[]) =>
    dispatch(actions.removeSelectedUsers(users)),
  incrementRow: (
    firstName: string,
    lastName: string,
    age: number,
    favouriteColor: string,
    captain: string
  ) =>
    dispatch(
      actions.incrementRow(firstName, lastName, age, favouriteColor, captain)
    ),
  decrementRow: (id: string) => dispatch(actions.decrementRow(id)),
  upUser: (id: string) => dispatch(actions.upUser(id)),
  downUser: (id: string) => dispatch(actions.downUser(id)),
  editUser: (id: string, editedUser: any) =>
    dispatch(actions.editUser(id, editedUser))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
