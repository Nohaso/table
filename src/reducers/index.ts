import { User } from "../components/Table"
import faker from "faker"
import { Reducer } from "redux"
import { v1 as uuid } from "uuid"
import * as types from "../constants/ActionTypes"
import { AppActions } from "../actions"

const setColor = () => {
  const number: number = Math.random() * 2
  let color: string = ""
  if (number < 0.25) color = "Red"
  else if (number < 0.5 && number > 0.25) {
    color = "Blue"
  } else if (number < 0.75 && number > 0.5) {
    color = "Yellow"
  } else if (number < 1 && number > 0.75) {
    color = "Green"
  } else if (number < 1.25 && number > 1) {
    color = "Black"
  } else if (number < 1.5 && number > 1.25) {
    color = "Pink"
  } else if (number < 1.75 && number > 1.5) {
    color = "Purple"
  } else if (number < 2 && number > 1.75) {
    color = "Brown"
  }
  return color
}

const generateUsers = (count: number): User[] => {
  const arr: any[] = []
  for (let idx = 0; idx <= count; idx++) {
    arr.push({
      id: uuid(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      age: Math.round(Math.random() * 100),
      favouriteColor: setColor()
    })
  }
  arr.map(user => {
    const filtred = arr.filter(element => {
      return element.id !== user.id
    })
    user["captain"] = faker.random.arrayElement(filtred).id
    return user
  })
  return arr
}

interface AppState {
  users: User[]
  selectedUsersIds: Array<string>
}
const initialState: AppState = {
  users: generateUsers(300),
  selectedUsersIds: []
}

const reducer: Reducer<AppState, AppActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case types.types.SELECT_USER:
      return {
        ...state,
        selectedUsersIds: [...state.selectedUsersIds, action.id]
      }
    case types.types.SELECT_USERS:
      // TODO: using Array.reduce
      let ids = action.users.reduce((acc, curr) => {
        if (acc.find(id => id === curr.id)) return acc
        else {
          acc.push(curr.id)
          return acc
        }
      }, state.selectedUsersIds)
      return { ...state, selectedUsersIds: ids }

    case types.types.REMOVE_SELECTED_USERS:
      // TODO: using Array.reduce
      let idxs = action.users.reduce((acc, curr) => {
        if (acc.find(id => id === curr.id)) {
          return acc.filter(id => id !== curr.id)
        }
        return acc
      }, state.selectedUsersIds)
      return { ...state, selectedUsersIds: idxs }

    case types.types.REMOVE_SELECTED_USER:
      return {
        ...state,
        selectedUsersIds: state.selectedUsersIds.filter(
          ids => ids !== action.id
        )
      }
    case types.types.INCREMENT_ROW:
      const user: User = {
        firstName: action.firstName,
        lastName: action.lastName,
        age: action.age,
        favouriteColor: action.favouriteColor,
        captain: action.captain,
        id: uuid()
      }
      return { ...state, users: [...state.users, user] }
    case types.types.DECREMENT_ROW:
      var index = state.users.findIndex(user => user.id === action.id)
      return {
        ...state,
        users: [
          ...state.users.slice(0, index),
          ...state.users.slice(index + 1, state.users.length)
        ]
      }
    case types.types.UP_USER:
      var index = state.users.findIndex(user => user.id === action.id)
      if (index > 0) {
        return {
          ...state,
          users: [
            ...state.users.slice(0, index - 1),
            ...state.users.slice(index, index + 1),
            ...state.users.slice(index - 1, index),
            ...state.users.slice(index + 1, state.users.length)
          ]
        }
      }
    case types.types.DOWN_USER:
      var index = state.users.findIndex(user => user.id === action.id)
      return {
        ...state,
        users: [
          ...state.users.slice(0, index),
          ...state.users.slice(index + 1, index + 2),
          ...state.users.slice(index, index + 1),
          ...state.users.slice(index + 2, state.users.length)
        ]
      }
    case types.types.EDIT_USER:
      return {
        ...state,
        users: state.users.map(user => {
          if (user.id === action.id) {
            return {
              id: action.id,
              firstName: action.editedUser.firstName,
              lastName: action.editedUser.lastName,
              age: action.editedUser.age,
              favouriteColor: action.editedUser.favouriteColor,
              captain: action.editedUser.captain
            }
          }
          return user
        })
      }
    default:
      return state
  }
}

export default reducer
