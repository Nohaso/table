import React, { Component } from "react";
import { RouteChildrenProps } from "react-router";
import { User } from "../Table";
import { Link } from "react-router-dom";

interface UserSquareProps {
  xPos: number;
  yPos: number;
  user: User;
  selectUser?: (id: string) => void;
  removeUser?: (id: string) => void;
}

export class UserSquare extends Component<UserSquareProps> {
  //vse v render

  handleClickAdd = (event: React.MouseEvent<SVGTextElement, MouseEvent>) => {
    if (this.props.selectUser) {
      this.props.selectUser(this.props.user.id);
    }
  };

  handleClickRemove = (event: React.MouseEvent<SVGTextElement, MouseEvent>) => {
    if (this.props.removeUser) {
      this.props.removeUser(this.props.user.id);
    }
  };

  render() {
    return (
      <g>
        <Link to={`/ref/${this.props.user.id}`}>
          <rect
            x={this.props.xPos - 75}
            y={this.props.yPos - 20}
            width="150"
            height="40"
            stroke="#aaa"
            stroke-width="2"
            fill="#fff"
          />
          <text
            x={this.props.xPos}
            y={this.props.yPos}
            alignment-baseline="middle"
            font-size="12"
            stroke-width="0"
            stroke="#000"
            text-anchor="middle"
          >{`${this.props.user.firstName} ${this.props.user.lastName} `}</text>
        </Link>
        {this.props.selectUser && (
          <text
            x={this.props.xPos + 55}
            y={this.props.yPos}
            alignment-baseline="middle"
            font-size="12"
            stroke-width="0"
            stroke="#000"
            text-anchor="middle"
            onClick={event => this.handleClickAdd(event)}
          >
            +
          </text>
        )}
        {this.props.removeUser && (
          <text
            x={this.props.xPos + 75}
            y={this.props.yPos}
            alignment-baseline="middle"
            font-size="12"
            stroke-width="0"
            stroke="#000"
            text-anchor="middle"
            onClick={event => this.handleClickRemove(event)}
          >
            -
          </text>
        )}
      </g>
    );
  }
}
