import * as React from "react"

interface PlaceAlongLineProps {
  items: any[]
  render: (props: { item: any; xPos: number; yPos: number }) => any
  start: { xPos: number; yPos: number }
  end: { xPos: number; yPos: number }
}

export class PlaceAlongLine extends React.Component<PlaceAlongLineProps> {
  xPos = (width: number, index: number, count: number): number => {
    if (count === 1) {
      return width / 2
    }
    let step = width / (count - 1)
    return step * index
  }

  yPos = (height: number, index: number, count: number): number => {
    if (count === 1) {
      return height / 2
    }
    let step = height / (count - 1)
    return step * index
  }

  render() {
    const { items, start, end, render } = this.props
    const components = items.map((item, index) => {
      return render({
        item,
        xPos:
          this.xPos(Math.abs(end.xPos - start.xPos), index, items.length) +
          start.xPos,
        yPos:
          this.yPos(Math.abs(end.yPos - start.yPos), index, items.length) +
          start.yPos
      })
    })
    return components
  }
}
