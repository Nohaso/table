import * as React from "react"
import { Link } from "react-router-dom"
import { User } from "../Table"

interface RowRender {
  user: User
  onRemove(): void
  onUp(): void
  onDown(): void
  users: User[]
}

export const Row: React.SFC<RowRender> = ({
  user,
  onRemove,
  onUp,
  onDown,
  users
}) => {
  let userExists = users.find(item => {
    return item.id === user.captain
  })
  let existUser = {
    firstName: "No",
    lastName: "Captain"
  }
  if (userExists !== undefined) {
    existUser = userExists
  }
  return (
    <tr>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>{user.age}</td>
      <td>{user.favouriteColor}</td>
      <td>{`${existUser.firstName} ${existUser.lastName}`}</td>
      <td>
        <button onClick={() => onRemove()}>delete User</button>
      </td>
      <td>
        <button onClick={() => onUp()}>Up User</button>
        <button onClick={() => onDown()}>Down User</button>
        <Link to={`/users/${user.id}`}>Edit</Link>
        <span> </span>
        <Link to={`/ref/${user.id}`}>Family</Link>
      </td>
    </tr>
  ) 
}