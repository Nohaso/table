import * as faker from "faker"
import { v1 as uuid } from "uuid"
import { Row } from "../index"

export const generateUsers = (count: number) => {
  const arr: any[] = []
  for (let idx = 0; idx <= count; idx++) {
    arr.push({
      id: uuid(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      age: Math.round(Math.random() * 100),
      favouriteColor: "black"
    })
  }
  arr.map(user => {
    const filtred = arr.filter(element => {
      return element.id !== user.id
    })
    user["captain"] = faker.random.arrayElement(filtred).id
    return user
  })
  return arr
}

const users = generateUsers(30)
const [user] = users

export default {
  component: Row,
  props: {
    user,
    users,
    onUp: () => console.log("called up")
  }
}
