import React, { Component } from "react"
import { User } from "../../Table"
import { GraphBar } from "../../GraphBar"
import { generateUsers } from "../../Row/__fixtures__"
import { ColumnGraph } from ".."

let users = generateUsers(150)
let width = 500
let height = 500

const groupBy = (objectArray: any) => {
  return objectArray.reduce((acc: any, obj: any) => {
    var key = obj["favouriteColor"]
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(obj)
    return acc
  }, {})
}

export default {
  component: ColumnGraph,
  state: {},
  props: {
    users,
    width,
    height,
    select: (users: User[]) => console.log("SelectedUsers: ", users),
    remove: (users: User[]) => console.log("UsersWereRemoved: ", users)
  }
}
