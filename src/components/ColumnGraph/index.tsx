import React, { Component } from "react"
import { User } from "../Table"
import { GraphBar } from "../GraphBar"

interface ColumnGraphProps {
  users: User[]
  width: number
  height: number
  select(users: User[]): void
  remove(users: User[]): void
  sortby(users: User[]): any
}

// TODO: create ColumnGraph component
//   // TODO: destruct vars

// TODO: using BarGraph
// GraphBar at ma osy x a y, popisky, pod popiskama at je + a -

export class ColumnGraph extends Component<ColumnGraphProps> {
  //vse v render
  render() {
    const { users, width, height, select, remove, sortby } = this.props
    let sortedUsers = sortby(this.props.users)
    let keys = Object.keys(sortedUsers).sort()
    return (
      <svg width={width} height={width}>
        {keys.map((key, index) => (
          <GraphBar
            key={key}
            colour={key}
            width={width / keys.length / 3}
            height={(sortedUsers[key].length / users.length) * height}
            xPos={50 + (index * width) / keys.length}
            yPos={height - (sortedUsers[key].length / users.length) * height}
            link={`/overview/colours/${key}`}
            select={() => select(sortedUsers[key])}
            remove={() => remove(sortedUsers[key])}
          />
        ))}
        <line
          x1={25}
          y1={500}
          x2={600}
          y2={500}
          stroke="black"
          stroke-width="1"
        />
        <line x1={30} y1={0} x2={30} y2={500} stroke="black" stroke-width="1" />
      </svg>
    )
  }
}
