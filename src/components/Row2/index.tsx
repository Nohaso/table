import * as React from "react"

interface Row2Render {
  user: any
}

export const Row2: React.SFC<Row2Render> = ({ user }) => {
  return (
    <tr>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>{user.age}</td>
      <td>{user.favouriteColor}</td>
    </tr>
  )
}
