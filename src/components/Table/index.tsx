import React, { Component } from "react"
import { Row } from "../Row"
import { Page } from "../Page"

export interface User {
  id: string
  firstName: string
  lastName: string
  age: number
  favouriteColor: string
  captain: string
}

interface CreateTableProps {
  users: User[]
  decrementRow: (id: string) => void
  upUser: (id: string) => void
  downUser: (id: string) => void
}
interface CreateTableState {
  currentPage: number
  usersPerPage: number
  captain: string
}

export class CreateTable extends Component<CreateTableProps, CreateTableState> {
  state = {
    currentPage: 1,
    usersPerPage: 20,
    captain: ""
  }

  handleClick = (number: number) => {
    this.setState({
      currentPage: number
    })
  }

  render() {
    const { users, decrementRow, upUser, downUser } = this.props
    const indexOfLastUser = this.state.currentPage * this.state.usersPerPage
    const indexOfFirstUser = indexOfLastUser - this.state.usersPerPage
    const currentUsers = users.slice(indexOfFirstUser, indexOfLastUser)

    return (
      <div className="App">
        <table className="Table">
          {currentUsers.map(user => (
            <Row
              user={user}
              onRemove={() => decrementRow(user.id)}
              onUp={() => upUser(user.id)}
              onDown={() => downUser(user.id)}
              users={users}
            />
          ))}
        </table>
        <Page
          users={users}
          perPage={this.state.usersPerPage}
          onPage={number => this.handleClick(number)}
        />
      </div>
    )
  }
}

/**
 * src/
 *  - components/
 *   - Table/
 *    - index.tsx
 *   - ColumnGraph/
 *    - index.tsx
 *
 * import ss from './src/components/Table'
 */
