import { Row } from "../../Row"
import { Page } from "../../Page"
import { generateUsers } from "../../Row/__fixtures__"
import { CreateTable } from "../index"

const users = generateUsers(30)

export default {
  component: CreateTable
  // props: {
  //   users,
  //   // decrementRow:(id:string)=>void,
  //   // upUser:(id:string)=>void,
  //   downUser:(id:string)=>void
  // }
}
