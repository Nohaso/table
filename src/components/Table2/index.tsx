import React, { Component } from "react"
import { Row2 } from "../Row2"
import { User } from "../Table"
import { RouteChildrenProps } from "react-router"

interface CreateTable2Props {
  users: User[]
  colours: boolean
}

const usersToArray = (users: User[]): User[] => {
  let usersArray: any[] = []
  let rows = 0
  users.forEach(user => {
    let fieldNum = 0
    for (let i = 0; i < users.length; i++) {
      if (user.id === users[i].captain) fieldNum++
    }
    if (fieldNum > rows) rows = fieldNum
  })
  for (let i = 0; i <= rows; i++) {
    usersArray.push([])
  }
  users.forEach(user => {
    let fieldNum = 0
    for (let i = 0; i < users.length; i++) {
      if (user.id === users[i].captain) fieldNum++
    }
    usersArray[fieldNum].push(user)
  })
  return usersArray
}

export class CreateTable2 extends Component<
  CreateTable2Props & RouteChildrenProps<{ users: string }>
> {
  render() {
    let users: any[]
    if (!this.props.match) {
      return <span>No one with this colour</span>
    }
    if (this.props.colours) {
      users = this.props.users.filter(
        user =>
          user.favouriteColor ===
          (this.props.match && this.props.match.params.users)
      )
    } else {
      let id = Number(this.props.match.params.users)
      users = usersToArray(this.props.users)
      users = users[id]
    }

    return (
      <div className="App">
        <table className="Table">
          {users.map(user => (
            <Row2 user={user} />
          ))}
        </table>
      </div>
    )
  }
}
