import React, { Component } from "react"

interface ClockProps {
  time?: string
}
interface State {
  time: string
}

class Clock extends Component<ClockProps, State> {
  state = { time: "" }

  componentDidMount() {
    let loadInterval = this.getTime()
  }

  getTime = () => {
    if (this.props.time) {
      this.setState({ time: this.props.time })
    } else {
      setInterval(() => {
        let d, h, m, s, t

        d = new Date()
        h = d.getHours()
        m = d.getMinutes()
        s = d.getSeconds()
        t = `${h}:${m}:${s}`
        this.setState({
          time: t
        })
      }, 1000)
    }
  }

  render() {
    return <div>{this.state.time}</div>
  }
}

export default Clock
