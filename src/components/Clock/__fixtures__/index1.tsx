import React, { Component } from "react"
import Clock from "../"

export default [
  {
    component: Clock,
    name: "Without props"
  },
  {
    component: Clock,
    name: "With props",
    props: { time: "12:38:57" }
  }
]
