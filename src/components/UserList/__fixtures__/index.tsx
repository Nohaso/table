import React, { Component } from "react"
import { RouteChildrenProps } from "react-router"
import { User } from "../../Table"
import { UserSquare } from "../../Square"
import { PlaceAlongLine } from "../../AlongLine"
import { SelectedUsersTable } from "../../SelectedTable"
import { CreateList } from ".."
import { generateUsers } from "../../Row/__fixtures__"

interface CreateListProps {
  users: User[]
  width: number
  height: number
  selectUser(id: string): void
  removeUser(id: string): void
  selectedUsersIds: string[]
}

const width = 500
const height = 500
const users: User[] = generateUsers(30)
const selectedUsersIds: string[] = []

export default {
  component: CreateList,
  props: {
    users,
    width,
    height,
    // selectUser,
    // removeUser,
    selectedUsersIds
  }
}
