import React, { Component } from "react"
import { RouteChildrenProps } from "react-router"
import { User } from "../Table"
import { UserSquare } from "../Square"
import { PlaceAlongLine } from "../AlongLine"
import { SelectedUsersTable } from "../SelectedTable"

//vycistit typy, poprepojovat connect redux, Componenta a pripojeni, napoj cosmos

interface CreateListProps {
  users: User[]
  width: number
  height: number
  selectUser(id: string): void
  removeUser(id: string): void
  selectedUsersIds: string[]
}

export class CreateList extends Component<
  CreateListProps & RouteChildrenProps<{ id: string }>
> {
  // selectUser = (id: string) => {};

  userIsAdded = (
    user: User,
    addedUsers: User[],
    xPos: number,
    yPos: number
  ) => {
    if (addedUsers.find(element => element.id === user.id)) {
      return (
        <UserSquare
          xPos={xPos}
          yPos={yPos}
          user={user}
          removeUser={this.props.removeUser}
        />
      )
    }
    return (
      <UserSquare
        xPos={xPos}
        yPos={yPos}
        user={user}
        removeUser={this.props.removeUser}
      />
    )
  }

  isSelected = (user: User, users: User[]): boolean => {
    const isSelected = users.find(element => element === user)
    if (isSelected) return true
    return false
  }
  //vse v render
  render() {
    if (!this.props.match) {
      return <span>You didnt choose user</span>
    }
    const user = this.props.users.find(
      element => element.id === (this.props.match && this.props.match.params.id)
    ) //jestli existuje tak se porovnava ID
    if (!user) {
      return <span>User does not exist</span>
    }
    const captain = this.props.users.find(
      element => element.id === user.captain
    )
    if (!captain) {
      return <span>User without captain</span>
    }
    let usersWithCap = this.props.users.filter(
      element => element.captain === user.id
    )

    const selectedUsers = this.props.selectedUsersIds.map(userid => {
      return this.props.users.find(user => user.id === userid) as User
    })

    return (
      <div>
        <div>
          <svg height={this.props.height} width={this.props.width}>
            <UserSquare
              xPos={this.props.width / 2}
              yPos={40}
              user={captain}
              selectUser={
                this.isSelected(captain, selectedUsers)
                  ? undefined
                  : this.props.selectUser
              }
              removeUser={
                this.isSelected(captain, selectedUsers)
                  ? this.props.removeUser
                  : undefined
              }
            />
            <UserSquare
              xPos={this.props.width / 2}
              yPos={100}
              user={user}
              selectUser={
                this.isSelected(user, selectedUsers)
                  ? undefined
                  : this.props.selectUser
              }
              removeUser={
                this.isSelected(user, selectedUsers)
                  ? this.props.removeUser
                  : undefined
              }
            />
            <PlaceAlongLine
              items={usersWithCap}
              start={{ xPos: 75, yPos: 200 }}
              end={{ xPos: this.props.width - 75, yPos: 550 }}
              render={props => (
                <UserSquare
                  xPos={props.xPos}
                  yPos={props.yPos}
                  user={props.item}
                  selectUser={
                    this.isSelected(props.item, selectedUsers)
                      ? undefined
                      : this.props.selectUser
                  }
                  removeUser={
                    this.isSelected(props.item, selectedUsers)
                      ? this.props.removeUser
                      : undefined
                  }
                />
              )}
            />
          </svg>
        </div>
        <div>
          <svg height={this.props.height / 3} width={this.props.width}>
            <SelectedUsersTable
              items={selectedUsers}
              width={600}
              height={400}
              render={props => (
                <UserSquare
                  user={props.item}
                  xPos={props.xPos}
                  yPos={props.yPos}
                  selectUser={
                    this.isSelected(props.item, selectedUsers)
                      ? undefined
                      : this.props.selectUser
                  }
                  removeUser={
                    this.isSelected(props.item, selectedUsers)
                      ? this.props.removeUser
                      : undefined
                  }
                />
              )}
            />
          </svg>
        </div>
      </div>
    )
  }
}

// pri pridani zmizi tlacitko +, a objevi se tlacitko minus, nepridany nebude mit -, dolni rada do vice radku po x userech, sirka,vyska SVG nastavitelna v props, radky HOC kompontent
