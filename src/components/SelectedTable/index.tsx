import * as React from "react";

interface SelectedUsersTableProps {
  items: any[];
  render: (props: { item: any; xPos: number; yPos: number }) => any;
  width: number;
  height: number;
}

export class SelectedUsersTable extends React.Component<
  SelectedUsersTableProps
> {
  userToArray = (width: number, items: any[]): any[] => {
    var perRow = Math.floor(width / 165);
    var arrayOfArrays = [];
    for (var i = 0; i < items.length; i += perRow) {
      arrayOfArrays.push(items.slice(i, i + perRow));
    }
    return arrayOfArrays;
  };

  render() {
    const usersArray = this.userToArray(this.props.width, this.props.items);
    const { render } = this.props;
    const components = usersArray
      .map((item, index) => {
        const renders = [];
        for (let i = 0; i < item.length; i++) {
          renders.push(
            render({
              item: item[i],
              xPos: 75 + i * 165,
              yPos: 20 + index * 60
            })
          );
        }
        return renders;
      })
      .flat();
    return components;
  }
}
