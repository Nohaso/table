import * as React from "react"
import { SelectedUsersTable } from ".."

interface SelectedUsersTableProps {
  items: any[]
  render: (props: { item: any; xPos: number; yPos: number }) => any
  width: number
  height: number
}

// export default {
//   component: SelectedUsersTable,
//   props: { items, render, width, height }
// }
