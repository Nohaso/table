import React, { Component } from "react";
import  {User} from '../Table';

interface Page {
  users: User[];
  perPage: number;
  onPage(number: number): void;
}

export const Page: React.SFC<Page> = ({ users, perPage, onPage }) => {
  const pageNumbers: number[] = [];
  for (let i: number = 1; i <= Math.ceil(users.length / perPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <div>
      {pageNumbers.map(number => (
        <button key={number} onClick={() => onPage(number)}>
          {number}
        </button>
      ))}
    </div>
  );
};
