import React, { Component } from "react"
import { RouteChildrenProps } from "react-router"
import { Link } from "react-router-dom"

interface GraphBarProps {
  colour: string
  width: number
  height: number
  xPos: number
  yPos: number
  link: string
  select(): void
  remove(): void
}

export class GraphBar extends Component<GraphBarProps> {
  handleClickRemove = (event: React.MouseEvent<SVGRectElement, MouseEvent>) => {
    this.props.remove()
  }
  handleClickSelect = (event: React.MouseEvent<SVGRectElement, MouseEvent>) => {
    this.props.select()
  }
  //vse v render
  render() {
    const { colour, width, height, xPos, yPos, link } = this.props
    return (
      <g>
        <defs>
          <linearGradient id={colour} x1="0%" y1="100%" x2="0%" y2="30%">
            <stop offset="0%" stopColor="white" stopOpacity="1" />
            <stop offset="100%" stopColor={colour} stopOpacity="1" />
          </linearGradient>
        </defs>
        <Link to={link}>
          <rect
            width={width}
            height={height}
            x={xPos}
            y={yPos - 100}
            fill={`url(#${colour}`}
          />
        </Link>
        <text x={xPos} y={550} fill={colour}>
          {colour}
        </text>
        <rect
          width={width}
          height={10}
          x={xPos}
          y={500}
          stroke="black"
          stroke-width="1"
          fill="white"
          onClick={event => this.handleClickSelect(event)}
        />
        <rect
          width={width}
          height={10}
          x={xPos}
          y={500 + 10}
          stroke="black"
          stroke-width="1"
          fill={colour}
          onClick={event => this.handleClickRemove(event)}
        />
      </g>
    )
  }
}
