import React, { Component, useReducer } from "react";
import { User } from "../Table";
import { RouteChildrenProps } from "react-router";

interface AddUserprops {
  addUser: (
    firstName: string,
    lastName: string,
    age: number,
    favouriteColor: string,
    captain: string
  ) => void;
  editUser: (
    id: string,
    editedUser: {
      firstName: string;
      lastName: string;
      age: number;
      favouriteColor: string;
      captain: string;
    }
  ) => void;
  users: User[];
}

interface State {
  firstName: string;
  firstNameError: string;
  lastName: string;
  lastNameError: string;
  age: number;
  ageError: string;
  favouriteColor: string;
  favouriteColorError: string;
  captain: string;
}

export class AddUser extends Component<
  AddUserprops & RouteChildrenProps<{ id: string }>,
  State
> {
  state = {
    firstName: "",
    firstNameError: "",
    lastName: "",
    lastNameError: "",
    age: 0,
    ageError: "",
    favouriteColor: "",
    favouriteColorError: "",
    captain: ""
  };

  handleChangefirstName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ firstName: event.target.value });
  };
  handleChangefavouriteColor = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    this.setState({ favouriteColor: event.target.value });
  };
  handleChangeLastName = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ lastName: event.target.value });
  };
  handleChangeAge = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.value);
    if (isNaN(event.target.value as any)) {
      this.setState({ age: 0 });
    } else {
      this.setState({ age: parseInt(event.target.value) });
    }
  };
  handleChangeCaptain = (event: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({ captain: event.target.value });
  };

  validate = () => {
    let firstNameError = "";
    let lastNameError = "";
    let ageError = "";
    let colorError = "";
    if (this.state.firstName.length < 3 || this.state.firstName.length > 20) {
      firstNameError =
        "First name must be at least 3 and less than 20 chars long";
    }
    if (this.state.lastName.length < 3 || this.state.lastName.length > 20) {
      lastNameError =
        "Last name must be at least 3 and less than 20 chars long";
    }
    if (this.state.age < 18 || this.state.age > 50) {
      ageError = "If you want Add to list you must be 18-50 years old";
    }
    if (this.state.favouriteColor.length === 0) {
      colorError = "Color must be choosen";
    }
    if (firstNameError || lastNameError || ageError || colorError) {
      this.setState({
        firstNameError: firstNameError,
        lastNameError: lastNameError,
        ageError: ageError,
        favouriteColorError: colorError
      });
      return false;
    }
    return true;
  };

  handleSubmit = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    const validate = this.validate();
    if (validate) {
      if (this.props.match && this.props.match.params.id) {
        this.props.editUser(this.props.match.params.id, this.state);
      } else {
        this.props.addUser(
          this.state.firstName,
          this.state.lastName,
          this.state.age,
          this.state.favouriteColor,
          this.state.captain
        );
      }
      this.setState({
        firstName: "",
        firstNameError: "",
        lastName: "",
        lastNameError: "",
        age: 0,
        favouriteColor: "",
        captain: ""
      });
    }
  };

  componentWillMount = () => {
    if (this.props.match && this.props.match.params.id) {
      const user = this.props.users.find(user => {
        if (user.id === (this.props.match && this.props.match.params.id)) {
          return true;
        }
        return false;
      });
      if (user) {
        this.setState({
          firstName: user.firstName,
          lastName: user.lastName,
          age: user.age,
          favouriteColor: user.favouriteColor,
          captain: user.captain
        });
      }
    }
  };

  render() {
    return (
      <div className="App">
        <div>
          <input
            type="text"
            id="firstName"
            placeholder="First Name"
            value={this.state.firstName}
            onChange={this.handleChangefirstName}
          />
          <span>{this.state.firstNameError}</span>
        </div>

        <div>
          <input
            type="text"
            id="lastName"
            placeholder="Last Name"
            value={this.state.lastName}
            onChange={this.handleChangeLastName}
          />
          <span>{this.state.lastNameError}</span>
        </div>
        <div>
          <span>Age: </span>
          <input
            type="text"
            id="age"
            placeholder=""
            value={this.state.age}
            onChange={this.handleChangeAge}
          />
        </div>
        <span>{this.state.ageError}</span>
        <div>
          <span>Select your favourite Color: </span>
          <select
            name="colours"
            value={this.state.favouriteColor}
            onChange={this.handleChangefavouriteColor}
          >
            <option value="" />
            <option value="red">Red</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
            <option value="yellow">Yellow</option>
          </select>
          <span>{this.state.favouriteColorError}</span>
        </div>
        <div>
          <span>Choose your captain: </span>
          <select
            name="Captain"
            value={this.state.captain}
            onChange={this.handleChangeCaptain}
          >
            <option value="" />
            {this.props.users.map(user => {
              return (
                <option value={user.id}>{`${user.firstName} ${
                  user.lastName
                }`}</option>
              );
            })}
          </select>
        </div>
        <div>
          <button onClick={event => this.handleSubmit(event)}>Add: </button>
        </div>
      </div>
    );
  }
}
